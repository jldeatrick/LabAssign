import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.io.File;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Collections;

public class LabAssign {

    private ArrayList<Assistant> assistants;    /* A list of all our wonderful Lab Assistants */
    private ArrayList<Section> sections; /* A list of all our also wonderful lab sections */
    private static final String[] dayList = {"Monday", "Tuesday", "Thursday", "Friday"};
    private ArrayList<Assignment> assignments;  /* the assignments compiled through our greedy algorithm */
    private Integer units;                      /* the total number of AI units we have to work with
                                                   for calculating proper AI-distributions across sections */

    private LabAssign(ArrayList<Assistant> assistants, ArrayList<Section> sections, Integer units) {
        this.assistants = assistants;
        this.units = units;
        this.assignments = new ArrayList<>();
        this.sections = sections;
        Collections.sort(assistants);
        Collections.sort(sections);
    }

    private void assignAll() {
        for (Assistant assistant : assistants) {
            /* for each unit the AI is signed up to receive, assign 2 sections */
            Collections.shuffle(sections);
            Collections.sort(sections);
            for (int i = 0; i < assistant.getUnits() * 2; i++) {
                for (Section section : sections) {
                    if (assistant.getAvailabilities().contains(section)) {
                        assignments.add(new Assignment(assistant, section));
                        assistant.getAvailabilities().remove(section);
                        section.decrementDemand();
                        break;
                    }
                }
//                System.out.println("Failed to assign " + assistant.getName() + "to a section for unit " + i);
            }
        }
    }


    private void writeCSV() {
        try {
            PrintWriter fwriter = new PrintWriter(new File("assignments.csv"));
            StringBuilder writer = new StringBuilder();
            writer.append("name");
            writer.append(',');
            writer.append("email");
            writer.append(',');
            writer.append("section");
            writer.append(',');
            writer.append("day");
            writer.append(",");
            writer.append("units wanted");
            writer.append(",");
            writer.append("hours wanted");
            writer.append("\n");
            for (Assignment assignment : assignments) {
                Assistant assistant = assignment.getAssistant();
                Section section = assignment.getSection();
                writer.append(assistant.getName());
                writer.append(',');
                writer.append(assistant.getEmail());
                writer.append(',');
                writer.append(section.getSection().toString());
                writer.append(',');
                String day = section.getDay();
                writer.append(day);
                writer.append(",");
                writer.append(assignment.getAssistant().unitsWanted);
                writer.append(",");
                writer.append(assignment.getAssistant().hoursWanted);
                writer.append('\n');
            }
            fwriter.write(writer.toString());
            fwriter.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /* takes in the absolute path of a file name and treats it as
        a csv, with columns as defined in the README and ouputs assignments */

    public static void main(String[] args) {
        String filename = "/home/joe/assistants.csv";

        ArrayList<Section> s = new ArrayList<>();
        ArrayList<Assistant> a = new ArrayList<>();
        Integer units = 0;

        /* inspiration for the bufferedreader implementation from here
           https://www.mkyong.com/java/how-to-read-and-parse-csv-file-in-java/ */

        String line;
        Integer total_units = 0;

        try (BufferedReader br = new BufferedReader(new FileReader(filename))) {

            String header = br.readLine();
            System.out.println(header);

            for (int j = 0; j < 4; j++) {
                String day = "ERROR";
                switch (j) {
                    case 0:
                        day = "Monday";
                        break;
                    case 1:
                        day = "Tuesday";
                        break;
                    case 2:
                        day = "Thursday";
                        break;
                    case 3:
                        day = "Friday";
                        break;
                    default:
                        System.out.println(day);
                        break;
                }
                s.add(new Section(101, day, "Kevin", 0));
                s.add(new Section(102, day, "Kelly", 0));
                s.add(new Section(103, day, "Catherine", 0));
                s.add(new Section(104, day, "Matt", 0));
                s.add(new Section(105, day, "Joe", 0));
                s.add(new Section(106, day, "Michael", 0));
                s.add(new Section(107, day, "Jackson", 0));
                s.add(new Section(108, day, "Lauren", 0));
                s.add(new Section(109, day, "Alex K", 0));
                s.add(new Section(110, day, "Alex H", 0));
                s.add(new Section(111, day, "Angela", 0));
            }


            while ((line = br.readLine()) != null) {

                String[] entry = line.split(","); /* format is {Timestamp, Name, ID, Email, Broken Futon, Rules, Unit Q, EPA Q,
                                                            Units wanted, Fall or Summer, hours wanted, availabilites, comments} */
                int i = 0;
//                for (String thing : entry) {
//                    thing = thing.replace("\"", "");
//                    System.out.println(i + ": " + thing);
//                    i++;
//                }
                String name = entry[1].replace("\"", "");
                ;
                String email = entry[3].replace("\"", "");
                ;
                String SID = entry[2].replace("\"", "");
                ;
                Integer units_wanted = Integer.parseInt(entry[8].replace("\"", ""));
                Integer hours_wanted = Integer.parseInt(entry[10].replace("\"", ""));
                if (units_wanted == 0) {
                    units_wanted = (int) Math.ceil(hours_wanted.doubleValue() / 3);
                }
                total_units += units_wanted;
                Assistant asst = new Assistant(name, email, SID, units_wanted, new ArrayList<>(), units_wanted, hours_wanted);
                String availabilities = entry[11].replace("\"", "");
                ;
                String[] section_times = availabilities.split(";");
                Integer section = -1;
                for (String time : section_times) {
                    String[] time_split = time.split(": ");
                    switch (time_split[1]) {
                        case "8-11 AM":
                            //kevin
                            asst.getAvailabilities().add(new Section(101, time_split[0], "Kevin", (int) Math.ceil(.08 * total_units * 2)));
                            break;
                        case "9-12 PM":
                            //me! :)
                            asst.getAvailabilities().add(new Section(105, time_split[0], "Joe", (int) Math.ceil(.09 * total_units * 2)));
                            break;
                        case "10-1 PM":
                            //angela on monday
                            System.out.println("THIS SHOULD BE A MONDAY: " + time_split[0]);
                            asst.getAvailabilities().add(new Section(111, time_split[0], "Angela", (int) Math.ceil(.08 * total_units * 2)));
                            break;
                        case "11-2 PM":
                            //Kelly or Angela
                            asst.getAvailabilities().add(new Section(111, time_split[0], "Angela", (int) Math.ceil(.08 * total_units * 2)));
                            asst.getAvailabilities().add(new Section(102, time_split[0], "Kelly", (int) Math.ceil(.09 * total_units * 2)));
                            break;
                        case "12-3 PM":
                            //Michael
                            asst.getAvailabilities().add(new Section(106, time_split[0], "Michael", (int) Math.ceil(.09 * total_units * 2)));
                            break;
                        case "2-5 PM":
                            //Catherine or Kazorian
                            asst.getAvailabilities().add(new Section(103, time_split[0], "Catherine", (int) Math.ceil(.12 * total_units * 2)));
                            asst.getAvailabilities().add(new Section(109, time_split[0], "Alex K", (int) Math.ceil(.09 * total_units * 2)));
                            break;
                        case "3-6 PM":
                            //Jackson
                            asst.getAvailabilities().add(new Section(107, time_split[0], "Jackson", (int) Math.ceil(.10 * total_units * 2)));
                            break;
                        case "5-8 PM":
                            //Matt or Hwang
                            asst.getAvailabilities().add(new Section(104, time_split[0], "Matt", (int) Math.ceil(.08 * total_units * 2)));
                            asst.getAvailabilities().add(new Section(110, time_split[0], "Alex H", (int) Math.ceil(.09 * total_units * 2)));
                            break;
                        case "6-9 PM":
                            //Lauren
                            asst.getAvailabilities().add(new Section(108, time_split[0], "Lauren", (int) Math.ceil(.09 * total_units * 2)));
                            break;
                        default:
                            System.out.println("WE SHOULD NOT BE HERE... " +
                                    "Section time was " + time_split[1]);
                            break;
                    }
                }

                a.add(asst);
            }
        } catch (
                IOException e)

        {
            e.printStackTrace();
        }

        LabAssign alg = new LabAssign(a, s, units);
        alg.assignAll();
        alg.writeCSV();

    }
}
