class Assignment {

    private Assistant assistant;
    private Section section;

    public Assignment(Assistant assistant, Section section) {
        this.assistant = assistant;
        this.section = section;
    }

    public void setAssistant(Assistant assistant) {
        this.assistant = assistant;
    }

    public void setSection(Section section) {
        this.section = section;
    }

    public Assistant getAssistant() {

        return assistant;
    }

    public Section getSection() {
        return section;
    }
}
