class Section implements Comparable{

    private Integer section;    /* section number as listed on the course website */
    private String day;        /* 0,1,2,3 corresponds to Mon,Tues,Th,Fri */
    private String TA;          /* the blear that talks at the front of the room */
    private Integer demand;     /* about how many AI's belong in this section */

    public Section(Integer section, String day, String TA, Integer demand) {
        this.section = section;
        this.day = day;
        this.TA = TA;
        this.demand = demand;
    }

    public void setSection(Integer section) {

        this.section = section;
    }

    public void setDay(String day) {
        this.day = day;
    }

    public void setTA(String TA) {
        this.TA = TA;
    }

    public void setDemand(Integer demand) {
        this.demand = demand;
    }

    public Integer getSection() {

        return section;
    }

    public String getDay() {
        return day;
    }

    public String getTA() {
        return TA;
    }

    public Integer getDemand() {
        return demand;
    }

    public void decrementDemand() {
        setDemand(getDemand() - 1);
    }

    @Override
    public int compareTo(Object other) {
        return ((Section) other).getDemand() - this.getDemand();
    }

    @Override
    public boolean equals(Object other){
        Section othersection = (Section) other;
        return othersection.getSection().equals(this.getSection()) &&
                othersection.getDay().equals(this.getDay()) &&
                othersection.getTA().equals(this.getTA());
    }
}
