import org.junit.Assert;
import org.junit.Test;

import java.util.ArrayList;
import java.util.Collections;

import static org.junit.Assert.assertEquals;

public class AssignTest {

    @Test
    /* tests that Assistants are sorting in increasing size of availability list*/
    public void testAssistantSorting() {
        ArrayList<Assistant> assistants = new ArrayList<>();
        Integer i = 0;
        while (i < 5) {
            ArrayList<Section> availabilities = new ArrayList<>();
            for (int j = i; j < 4; j++) {
//                Section s = new Section(100 + i, j, "Christine", 5);
//                availabilities.add(s);
            }
//            Assistant blear = new Assistant("blear" + i.toString(), "cs61b@berkeley.edu", "61B", 0, availabilities);
//            i++;
//            assistants.add(blear);
        }
        Collections.sort(assistants);
        for (int k = 0; k < 5; k++) {
            assertEquals(assistants.get(k).getName(), "blear" + (4 - k));
        }
    }

    @Test
    /* tests that Sections are sorting by decreasing demand */
    public void testSectionSorting() {
        ArrayList<Section> sections = new ArrayList<>();
        for (int i = 0; i < 4; i++) {
//            Section s = new Section(100 + i, i, "Christine", 1 + i);
//            sections.add(s);
        }
        Collections.sort(sections);
        for (int j = 0; j < sections.size(); j++)
        {
            assertEquals((int) sections.get(j).getSection(), 103 - j);
        }
    }

//    @Test
    /* make sure the CSV writing is performing as expected */


//    public void csvTest(){
//        ArrayList<Assignment> assignments = new ArrayList<>();
//        for (int i = 0; i < 3; i++){
//            Section s1 = new Section(100 + i, i, "Christine", 1);
//            Section s2 = new Section(100 + i, i+1, "Kevin", 1);
//            Assistant blear = new Assistant("blear" + i, "cs61b@berkeley.edu", "61B", 0, null);
//            Assignment assign1 = new Assignment(blear, s1);
//            Assignment assign2 = new Assignment(blear, s2);
//            assignments.add(assign1);
//            assignments.add(assign2);
//        }
//        LabAssign alg = new LabAssign(new ArrayList<>(), new ArrayList<>(), 0);
//        alg.assignments = assignments;
//        alg.writeCSV();
//    }

}
