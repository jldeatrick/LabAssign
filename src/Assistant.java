import java.util.ArrayList;

public class Assistant implements Comparable {

    private String name;
    private String email;
    private String SID;
    private Integer units;
    private ArrayList<Section> availabilities;
    public int unitsWanted;
    public int hoursWanted;

    public Assistant(String name, String email, String SID, Integer units, ArrayList<Section> availabilities, int units_wanted, int hours_wanted) {
        this.name = name;
        this.email = email;
        this.SID = SID;
        this.units = units;
        this.availabilities = availabilities;
        this.hoursWanted = hours_wanted;
        this.unitsWanted = units_wanted;
    }

    public void setName(String name) {

        this.name = name;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public void setSID(String SID) {
        this.SID = SID;
    }

    public void setUnits(Integer units) {
        this.units = units;
    }

    public void setAvailabilities(ArrayList<Section> availabilities) {
        this.availabilities = availabilities;
    }

    public String getName() {

        return name;
    }

    public String getEmail() {
        return email;
    }

    public String getSID() {
        return SID;
    }

    public Integer getUnits() {
        return units;
    }

    public ArrayList<Section> getAvailabilities() {
        return availabilities;
    }

    @Override
    public int compareTo(Object other) {
        return this.getAvailabilities().size() - ((Assistant) other).getAvailabilities().size();
    }
}
